Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  post '/signup', to: 'users#create'
  resources :logins, only: :create
  resources :books, only: :index
  resources :line_items, only: [:create, :destroy] do
    member do
      patch 'increase_quantity'
      patch 'decrease_quantity'
    end
  end
  resources :carts, only: :show do
    post 'clear_cart'
  end
  resources :orders, only: :create
end
