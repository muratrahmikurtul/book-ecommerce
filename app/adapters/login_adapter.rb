# frozen_string_literal: true

class LoginAdapter
  include Wisper::Publisher

  def login_user(user_params)
    username = user_params[:username]
    user = User.where(username: username).first

    unless user.present?
      broadcast(:user_not_found)
      return
    end

    if user.authenticate(user_params[:password])
      token, refresh_token = generate_tokens(user.id)
      broadcast(:successful_login, user, token, refresh_token)
    else
      broadcast(:failed_login)
    end
  end

  def generate_tokens(user_id)
    [
      AuthJsonWebToken::JsonWebToken.encode(user_id, 1.day.from_now, token_type: 'login'),
      AuthJsonWebToken::JsonWebToken.encode(user_id, 1.year.from_now, token_type: 'refresh')
    ]
  end
end
