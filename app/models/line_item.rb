# frozen_string_literal: true

class LineItem < ApplicationRecord
  belongs_to :book
  belongs_to :cart
  belongs_to :order, optional: true

  def total_price
    return 0.0 if book.nil? || quantity.nil?

    quantity * book.price
  end
end
