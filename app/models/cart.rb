# frozen_string_literal: true

class Cart < ApplicationRecord
  belongs_to :user
  has_many :line_items
  has_many :books, through: :line_items

  def sub_total
    line_items.map(&:total_price).sum
  end
end
