# frozen_string_literal: true

class Order < ApplicationRecord
  belongs_to :user
  has_many :line_items, dependent: :destroy

  before_create :set_total_price

  private

  def set_total_price
    self.total_price = line_items.map(&:total_price).sum
  end
end
