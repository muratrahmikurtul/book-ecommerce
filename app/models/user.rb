# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password
  validates :username, presence: true, uniqueness: true
  has_one :cart, dependent: :destroy
  has_many :orders, dependent: :destroy
end
