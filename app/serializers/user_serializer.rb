# frozen_string_literal: true

class UserSerializer < BaseSerializer
  attributes :id, :username

  has_one :cart, serializer: CartSerializer
end
