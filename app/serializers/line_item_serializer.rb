# frozen_string_literal: true

class LineItemSerializer < BaseSerializer
  attributes :id, :quantity, :total_price

  belongs_to :book, serializer: BookSerializer
  belongs_to :cart, serializer: CartSerializer
end
