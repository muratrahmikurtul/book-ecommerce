# frozen_string_literal: true

class CartSerializer < BaseSerializer
  attributes :id, :sub_total

  has_many :line_items, serializer: LineItemSerializer
  has_many :books, through: :line_items, serializer: BookSerializer
end
