# frozen_string_literal: true

class BookSerializer < BaseSerializer
  attributes :id, :title, :description, :price
end
