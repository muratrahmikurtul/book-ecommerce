# frozen_string_literal: true

class OrderSerializer < BaseSerializer
  attributes :id, :address, :total_price

  belongs_to :user, serializer: UserSerializer
  has_many :line_items, serializer: LineItemSerializer
end
