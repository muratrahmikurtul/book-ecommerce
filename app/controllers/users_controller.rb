# frozen_string_literal: true

class UsersController < ApplicationController
  def create
    user = User.where(username: params[:username]).first
    return render json: { errors: 'User already exists' }, status: :conflict if user

    if user_params[:password] != user_params[:password_confirmation]
      return render json: { errors: 'Password and password confirmation do not match' }, status: :bad_request
    end

    @user = User.new(user_params)

    if @user.save
      render json:  {
        message: "User successfully registered, you can login now."
      }, status: :created
    else
      render json: { errors: @user.errors.full_messages }, status: :unprocessable_entity
    end
  end

  private

  def encode(id)
    AuthJsonWebToken::JsonWebToken.encode(id)
  end

  def user_params
    params.permit(:username, :password, :password_confirmation)
  end
end
