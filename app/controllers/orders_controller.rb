# frozen_literal_string: true

class OrdersController < ApplicationController
  before_action :validate_json_web_token

  def create
    order = Order.new(order_params)
    order.line_items = current_user.cart.line_items
    order.user_id = current_user.id

    if order.save
      render json: { order: OrderSerializer.new(order, include: %i[line_items user]).serializable_hash }
    else
      render json: { errors: order.errors.full_messages }, status: :unprocessable_entity
    end
  end

  private

  def order_params
    params.require(:order).permit(:address)
  end
end
