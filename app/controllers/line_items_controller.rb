# frozen_literal_string: true

class LineItemsController < ApplicationController
  before_action :validate_json_web_token

  def create
    selected_book = Book.find(params[:book_id])
    add_to_cart(selected_book)
  end

  def destroy
    line_item = current_user.cart.line_items.find_by(id: params[:id])
    if line_item.destroy
      render json: { cart: CartSerializer.new(current_user.cart).serializable_hash }
    else
      render json: {
        errors: [{
                   delete_line_item: 'Error deleting line item',
                 }],
      }, status: :unprocessable_entity
    end
  end

  def increase_quantity
    line_item = current_user.cart.line_items.find_by(id: params[:id])
    return render json: { "errors": [{ "decrease_quantity": "Line item not found" }] }, status: :not_found if line_item.nil?

    line_item.quantity += 1
    if line_item.save
      render json: { cart: CartSerializer.new(current_user.cart, { include: %i[line_items books] }).serializable_hash }
    else
      render json: {
        errors: [{
                   increase_quantity: 'Error increasing quantity',
                 }],
      }, status: :unprocessable_entity
    end
  end

  def decrease_quantity
    line_item = current_user.cart.line_items.find_by(id: params[:id])
    return render json: { "errors": [{ "decrease_quantity": "Line item not found" }] }, status: :not_found if line_item.nil?

    if line_item.quantity > 1
      line_item.quantity -= 1
      if line_item.save
        render json: { cart: CartSerializer.new(current_user.cart, { include: %i[line_items books] }).serializable_hash }
      else
        render json: {
          errors: [{
                     decrease_quantity: 'Error decreasing quantity',
                   }],
        }, status: :unprocessable_entity
      end
    else
      render json: {
        errors: [{
                   decrease_quantity: 'Quantity cannot be less than 1',
                 }],
      }, status: :unprocessable_entity
    end
  end

  private

  def add_to_cart(selected_book)
    if current_user.cart.books.include?(selected_book)
      line_item = current_user.cart.line_items.find_by(book_id: selected_book.id)
      line_item.quantity += 1
    else
      line_item = LineItem.new
      line_item.book = selected_book
      line_item.cart = current_user.cart
      line_item.quantity = 1
      if line_item.save
        render json: { cart: CartSerializer.new(current_user.cart, { include: %i[line_items books] }).serializable_hash }
      else
        render json: {
          errors: [{
                     add_to_cart: 'Error adding book to cart',
                   }],
        }, status: :unprocessable_entity
      end
    end
  end
end
