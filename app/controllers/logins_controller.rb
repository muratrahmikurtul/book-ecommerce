# frozen_string_literal: true

class LoginsController < ApplicationController
  def create
    user = OpenStruct.new(params)
    output = ::LoginAdapter.new

    output.on(:user_not_found) do |user|
      render json: {
        errors: [{
                   user_not_found: 'User is not found, please sign up',
                 }],
      }, status: :unprocessable_entity
    end

    output.on(:failed_login) do |user|
      render json: {
        errors: [{
                   failed_login: 'Invalid username and/or password',
                 }],
      }, status: :unauthorized
    end

    output.on(:successful_login) do |user, token, refresh_token|
      handle_cart(user)
      render json: {meta: {
        token: token,
        refresh_token: refresh_token,
        user: UserSerializer.new(user)
      }}
    end

    output.login_user(user)
  end

  def destroy
    @token
    render json: { message: 'Logged out successfully' }, status: :ok
  end

  private

  def user_params
    params.permit(:username, :password)
  end

  def handle_cart(user)
    Cart.find_or_create_by(user_id: user.id)
  end
end
