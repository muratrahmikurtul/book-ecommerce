# frozen_string_literal: true

class BooksController < ApplicationController
  before_action :validate_json_web_token, only: [:index]

  def index
    books = Book.paginate(page: params[:page], per_page: 10)
    render json: { books: BookSerializer.new(books).serializable_hash }
  end
end
