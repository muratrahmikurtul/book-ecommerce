# frozen_string_literal: true

class CartsController < ApplicationController
  before_action :validate_json_web_token

  def show
    cart = Cart.find_by(user_id: current_user.id)
    options = { include: %i[line_items books] }

    render json: { cart: CartSerializer.new(cart, options).serializable_hash }
  end

  def clear_cart
    cart = Cart.find_by(user_id: current_user.id)
    cart.line_items.destroy_all

    render json: { cart: CartSerializer.new(cart).serializable_hash }
  end
end
