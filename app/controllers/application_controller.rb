class ApplicationController < ActionController::Base
  include Auth::JsonWebTokenValidation
  protect_from_forgery unless: -> { request.format.json? }

  def current_user
    @current_user ||= User.find(@token.id)
  rescue ActiveRecord::RecordNotFound
    render json: {
      errors: [{message: "Authentication token invalid"}]
    }, status: :unprocessable_entity
  end
end
